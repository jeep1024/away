define(function(require, exports, module)
{
	'use strict';

	var Defines =
	{
		  widthOptions: 240
		, demo: 0
		, numColumns: 3
		, numRows: 3
		, symbolPath: '/content/symbols/'
		, symbolExt: '.svg'
		, cmd:
		{
			  showGrid: 'showGrid'
			, resize: 'resize'
		}
		, msg:
		{
			  clickedSettings: 'clickedSettings'
			, clickedTile: 'clickedTile'
			, getSymbol: 'getSymbol'
			, selectItem: 'selectItem'
			, assetsLoaded: 'assetsLoaded'
			, getValue: 'getValue'
			, setValue: 'setValue'
		}
		, weather:
		{
			providers:
			{
				openweathermap:
				{
					  apiKey: '3e8cd3ec409f9312bb482fee7e9b2927'
					, intervalUpdate: 3 * 60 * 60 * 1000
					, intervalRetry: 10 * 60 * 1000
					, baseUrl: 'http://api.openweathermap.org/data/2.5/'
				}
			}
		}
	};

	// Run-time settings
	Defines.eventDown = (typeof document.body.ontouchstart === 'undefined') ? 'mousedown' : 'touchstart';
	
	module.exports = Defines;
});
