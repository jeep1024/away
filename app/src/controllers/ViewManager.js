define(function(require, exports, module)
{
    'use strict';

    // import dependencies
    var Defines = require('Defines');
    var Utils = require('lib/Utils');
    var Engine = require('famous/core/Engine');
    var Transform = require('famous/core/Transform');
    var StateModifier = require('famous/modifiers/StateModifier');
    var Modifier = require('famous/core/Modifier');
    var Transitionable = require('famous/transitions/Transitionable');
    var FastClick = require('famous/inputs/FastClick');
	
	var MainView = require('views/MainView');
	var SettingsView = require('views/SettingsView');
	//var Overlay = require('views/Overlay');
	var WeatherLoader = require('models/WeatherLoader');

	var c = Defines.cmd;
	var m = Defines.msg;
	
	
	function ViewManager()
	{
        // aliases
        var dbg = Utils.dbg('ViewManager');

        // state
        var mainContext;
        var optionsOpen = false;
		var tiled = false;
		var weatherLoader = new WeatherLoader(this);

        // ui
		//var overlay;
        var viewMain;
        var viewSettings;

		// settings
        var options =
        {
            openPosition: Defines.widthOptions,
            transition: {
                duration: 300,
                curve: 'easeOut'
            },
            posThreshold: 138,
            velThreshold: 0.75
        };

		dbg('obj=' + (typeof { foo:'bar' }));
		dbg('str=' + (typeof "foobar"));
		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.init = function()
        {
			weatherLoader.init({ city: 'Bothell', stateCountry: 'wa' });
			
            // create the main context
            mainContext = Engine.createContext();
			//mainContext.setPerspective(10000);

			// Add all of the views
			viewMain = new MainView();
			viewMain.run(this);
			viewSettings = new SettingsView();
			viewSettings.run(this);
			//overlay = new Overlay();
			//overlay.run(this);

            var size = viewMain.getSize();

            viewMain.state = new StateModifier({ /*transform:Transform.translate(0,0,11),*/ size: size });
            viewMain.pos = new Transitionable(0);
            viewMain.mod = new Modifier({ transform: function()
										 {
											 return Transform.translate(viewMain.pos.get(), 0, 0);
										 }
										});

            var dw = $(window).width();   // FIXME: Bad way to calculate size... find a better (relative) way
            viewSettings.state = new StateModifier({ size: viewSettings.getSize() });
            viewSettings.pos = new Transitionable(dw);
            viewSettings.mod = new Modifier({ transform: function()
											 {
												 return Transform.translate(viewSettings.pos.get(), 0, 0);
											 }
											});

           	mainContext.add(viewMain.mod).add(viewMain.state).add(viewMain);
			
			if (!Defines.demo)
			{
            	mainContext.add(viewSettings.mod).add(viewSettings.state).add(viewSettings);
				//mainContext.add(overlay);
			}
        };

        this.notify = function(msg, args)
        {
            switch (msg)
            {
                case m.clickedSettings:
                {
                    optionsOpen = !optionsOpen;
                    var dw = $(window).width();   // FIXME: Bad way to calculate size... find a better (relative) way
                    if (optionsOpen)
                    {
                        //dbg(' diff=' + (viewMain.getSize()[0]) + '/' + $(window).width() + ' -- ' + dw + ' -- ' + viewSettings.getSize()[0] + ' -- ' + (dw - viewSettings.getSize()[0]));
                        viewMain.pos.set(-options.openPosition, options.transition);//, function() { dbg('Completed open'); });
                        viewSettings.pos.set(dw - viewSettings.getSize()[0], options.transition);//, function() { dbg('Completed open'); });
                    }
                    else
                    {
                        viewMain.pos.set(0, options.transition);//, function() { dbg('Completed close'); });
                        viewSettings.pos.set(dw, options.transition);//, function() { dbg('Completed close'); });
                    }

                    break;
                }
				
				case m.clickedTile:
				{
					tiled = !tiled;
					viewMain.cmd(c.showGrid, tiled);
					break;
				}

				case m.getValue:
				{
					// For now, just use localStorage for persisted data
					return window.localStorage.getItem(args);
				}

				case m.setValue:
				{
					// For now, just use localStorage for persisted data
					var data = args.data;
					return window.localStorage.setItem(args.id, ((typeof data) === 'string') ? data : JSON.stringify(data));
				}
					
                default:
                {
                    dbg('Unknown msg "' + msg + '"', args);
                    break;
                }
            }

            return null;
        };
	}

	module.exports = ViewManager;
});
