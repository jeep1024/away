define(function(require, exports, module) {
    'use strict';

	var Defines = require('Defines');
	var Utils = require('lib/Utils');
	
    // import dependencies
	var View = require('famous/core/View');
    var Surface = require('famous/core/Surface');
	
	// components

	// main export
	function Overlay()
	{
		// aliases
		var c = Defines.cmd;
		var m = Defines.msg;
		var dbg = Utils.dbg('Overlay');
		
        // state
        var controller;

        // views

        // settings
        var size = [ undefined, undefined ];

		// ctor
		View.apply(this, arguments);
		
        // Define the overall size for hosting context modifier update
        this.setOptions({size: size});

		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.run = function(host)
        {
            controller = host;
        };
		
		// From controller
		this.cmd = function(cmd, args)
		{
			switch (cmd)
			{
				default:
				{
					dbg('Unknown cmd "' + cmd + '"', args);
					break;
				}
			}
			
			return null;
		};
		
		// From child controls
		this.notify = function(msg, args)
		{
			switch (msg)
			{
				default:
				{
					dbg('Unknown msg "' + msg + '"', args);
					break;
				}
			}
			
			return null;
		};
	}

    // Link to Famo.us View baseclass
    Overlay.prototype = Object.create(View.prototype);
    Overlay.prototype.constuctor = Overlay;

	module.exports = Overlay;
});
