define(function(require, exports, module) {
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var Utils = require('lib/Utils');
	
	// famo.us
	var View = require('famous/core/View');
    var Surface = require('famous/core/Surface');
    var Transform = require('famous/core/Transform');
    var Modifier = require('famous/core/Modifier');
    var StateModifier = require('famous/modifiers/StateModifier');
	
	// components
    var MainCanvas = require('views/mainView/MainCanvas');
	var SelectSettings = require('views/mainView/SelectSettings');
	var SelectTile = require('views/mainView/SelectTile');

	// main export
	function MainView()
	{
		// aliases
		var c = Defines.cmd;
		var m = Defines.msg;
		var dbg = Utils.dbg('MainView');
		
        // state
        var controller;

        // views
		var bg;
        var canvas;
        var btnSettings;
		var btnTile;

        // settings
        var size = [ undefined, undefined ];

		// ctor
		View.apply(this, arguments);
		
        // Define the overall size for hosting context modifier update
        this.setOptions({size: size});

		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.run = function(host)
        {
            controller = host;

            bg = new Surface(
            {
                size: size,
                properties: {
                    backgroundColor: '#231f20',
                    pointerEvents: 'none'
                }
            });

			canvas = new MainCanvas();
			canvas.run(this);

			this.add(bg);
			this.add(canvas);
        };
		
		// From controller
		this.cmd = function(cmd, args)
		{
			switch (cmd)
			{
				case c.showGrid:
				{
					return canvas.cmd(cmd, args);
				}
				
				default:
				{
					dbg('Unknown cmd "' + cmd + '"', args);
					break;
				}
			}
			
			return null;
		};
		
		// From child controls
		this.notify = function(msg, args)
		{
			switch (msg)
			{
				case m.getValue:
				case m.setValue:
				case m.clickedSettings:
				case m.clickedTile:			// From button, or item select in grid view
				{
					return controller.notify(msg, args);
				}
				
				case m.assetsLoaded:
				{
					//dbg(msg);
					if (!Defines.demo)
					{
						btnSettings = new SelectSettings();
						btnSettings.run(this);
						btnTile = new SelectTile();
						btnTile.run(this);
					
						this.add(btnSettings);
						this.add(btnTile);
					}
					
					break;
				}
				
				default:
				{
					dbg('Unknown msg "' + msg + '"', args);
					break;
				}
			}
			
			return null;
		};
	}

    // Link to Famo.us View baseclass
    MainView.prototype = Object.create(View.prototype);
    MainView.prototype.constuctor = MainView;

	module.exports = MainView;
});
