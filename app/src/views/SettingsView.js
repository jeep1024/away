define(function(require, exports, module)
{
    'use strict';

    // import dependencies
	var View = require('famous/core/View');
    var Modifier = require('famous/core/Modifier');
    var Transform = require('famous/core/Transform');
    var Surface = require('famous/core/Surface');
    var ImageSurface = require('famous/surfaces/ImageSurface');
    var Defines = require('Defines');

	function SettingsView()
	{
        // state
        var controller;

        // ui
        var bg;
        var hdr;
        var items = [];

        // settings
        var size = [ Defines.widthOptions, undefined ];
        var sizeHeader = [ undefined, 40 ];

        // ctor
		View.apply(this, arguments);

        // Define the overall size for hosting context modifier update
        this.setOptions({size: size});

        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.run = function(host)
        {
            controller = host;

            bg = new Surface(
            {
                size: size,
                properties: {
                    backgroundColor: '#1a1619',
                    boxShadow: 'inset 5px 1px 12px -6px rgba(0,0,0,1.0)',
                    pointerEvents: 'none'
                }
            });

            hdr = new Surface(
            {
                size: sizeHeader,
                content: '<div><span style="vertical-align:top;font-size:24px;font-weight:bold;border-bottom:1px solid #e0e0e0;">App Nav Menu</span></div>',
                properties: {
                    color: 'white',
                    fontFamily: 'freightsans_prolight_italic',
                    textAlign: 'center',
                    //backgroundColor: '#404040',
                    boxShadow: 'inset 5px 1px 12px -6px rgba(0,0,0,1.0)',
                    //textTransform: 'uppercase',
                    pointerEvents: 'none',
                    display: 'table-cell',
                    verticalAlign: 'middle',
                    lineHeight: sizeHeader[1] + 'px'
                }
            });

            this.add(bg);
            this.add(hdr);
        };
	}

    // Link to Famo.us View baseclass
	SettingsView.prototype = Object.create(View.prototype);
	SettingsView.prototype.constuctor = SettingsView;

	module.exports = SettingsView;
});
