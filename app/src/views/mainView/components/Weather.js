define(function(require, exports, module) {
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var Utils = require('lib/Utils');

	function Weather()
	{
        // state
		var angleCurrent = 0;
		var angleUpdate;
        var controller;
		var currWidth;
		var currHeight;
		var currGridWidth;
		var currGridHeight;
		var hasCompass = false;
		var idWatchCompass;

        // Full view elements
		var weatherFull;

		// Grid view elements
		var weatherGrid;
		
        // settings

        // consts
		var m = Defines.msg;
        var sec60 = (60 * 1000);
        var angSec = 360 / sec60;
        var offset = 7 * 60 * sec60;
		var dbg = Utils.dbg('Weather');
		var cUiText = 'uiText';
		var cUiTextAlt = 'uiTextAlt';
		var dirLabels = [
			  { l: 'N', p: 0 }
			, { l: 'NE', p: 45 }
			, { l: 'E', p: 90 }
			, { l: 'SE', p: 135 }
			, { l: 'S', p: 180 }
			, { l: 'SW', p: 225 }
			, { l: 'W', p: 270 }
			, { l: 'NW', p: 315 }
		];
		var dirMarkers = [
			{ txt: 'N', clr: '#FF2954', font: 'freightsans_probold' }
			, { txt: 'E', clr: '#FFFFFF', font: 'freightsans_promedium' }
			, { txt: 'S', clr: '#FFFFFF', font: 'freightsans_promedium' }
			, { txt: 'W', clr: '#FFFFFF', font: 'freightsans_promedium' }
		];
		

        ///////////////////////////////////////////////////////////////////////////////
        // PROPERTIES
        ///////////////////////////////////////////////////////////////////////////////
		
		this.getName = function()
		{
			return 'Weather';
		};
		
		this.setSize = function(w, h, gridWidth, gridHeight)
		{
			currWidth = w;
			currHeight = h;
			currGridWidth = gridWidth;
			currGridHeight = gridHeight;
		};
		
		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

		//
		// Generate full-view UI
		//
        this.generateFullView = function(host, scope, options, layerFull)
        {
            controller = host;
			weatherFull = generateCompass(scope, layerFull, Math.min(currWidth / 320, currHeight / 480), true, true);
			weatherFull.container.setPosition(new scope.Point(0 * currWidth / 2, 20));//currHeight / 2 - 0*10));
        };
		
		
		//
		// Generate grid-view UI
		//
		this.generateGridView = function(scope, layerGrid, gridWidth, gridHeight)
		{
			currGridHeight = gridHeight;
			currGridWidth = gridWidth;
			
			weatherGrid = generateCompass(scope, layerGrid, Math.min(currGridWidth / 320, currGridHeight / 480), false, false);
			weatherGrid.container.setPosition(new scope.Point(gridWidth / 2, gridHeight / 2 - 0*10));
		};
		
		
		//
		// Tick updates
		//
        this.update = function(e)
        {
			updateUi();
		};

		
        ///////////////////////////////////////////////////////////////////////////////
        // INTERNAL
        ///////////////////////////////////////////////////////////////////////////////

		// Start watching the compass
		function startCompass()
		{
			// FIXME: Detect if on iOS to allow for filter option
			var opts = { filter: 1.0 };//(isIos) ? { filter: 1.0 } : { frequency: 1000 };	// on iOS, filter = change in angle to signal an update
			idWatchCompass = navigator.compass.watchHeading(onCompassSuccess, onCompassError, opts);
		}
		
		// Stop watching the compass
		function stopCompass()
		{
			if (idWatchCompass)
			{
				navigator.compass.clearWatch(idWatchCompass);
				idWatchCompass = null;
			}
		}
		
		function generateCompass(scope, layer, scale, checkForCompass, addNews)
		{
            var i, len, s, dir, text;
            var rad = 2 * Math.PI / 360;
            var rLen = 120 * scale;
            var ang = -90;
            var angAlt = -90;
            var angSecond = -90;
            //var start = new scope.Point(0, 0);

            // Inner rotation container
			var inner = new scope.Layer();
			inner.locked = true;		// ignore hittest
			inner.transformContent = false;
			
            // NEWS markers
			if (addNews)
			{
				len = dirMarkers.length;
				for (i = 0; i < len; i++)
				{
					dir = dirMarkers[i];
					text = new scope.PointText({
						content: dir.txt,
						fillColor: dir.clr,
						font: dir.font + ', sans-serif',
						justification: 'center',
						fontSize: 24 * scale,
						rotation: 90 * i
					});

					s = 1.2;
					text.position = [ rLen * s * Math.cos(2 * Math.PI * i / 4 - Math.PI / 2)
									, rLen * s * Math.sin(2 * Math.PI * i / 4 - Math.PI / 2)];
				}
			}

            // Ticks
            for (i = 0; i < 72; i++)
            {
                s = (i % 6) ? 0.92 : 0.825;
                var tick = new scope.Path({
                    segments: [
                              [ rLen * Math.cos(rad * i * 5), rLen * Math.sin(rad * i * 5) ]
                            , [ rLen * s * Math.cos(rad * i * 5),  rLen * s * Math.sin(rad * i * 5) ]],
                    strokeColor: 'rgba(255,255,255,' + ((i % 6) ? '0.25' : '1') + ')',
                    strokeWidth: 2 * scale,//(i % 6) ? 2 : 4,
					strokeCap: 'square'//(i % 6) ? 'square' : 'round'
                });
            }
			
			var scaling = [ 0.5 * scale, 0.5 * scale ];
			
			// Inner arrows
			var sun = controller.notify(m.getSymbol, 'sun');
			//var sSun = sun.place(new scope.Point(0, -(rLen * 0.69)), false);
			var sSun = sun.place(new scope.Point(0, 0), false);
			sSun.scaling = scaling;
			sSun.position = new scope.Point(0, -(rLen * 0.69));
			var snow = controller.notify(m.getSymbol, 'snow');
			//var sSnow = snow.place(new scope.Point(0, (rLen * 0.69)));
			var sSnow = snow.place(new scope.Point(0, 0));
			sSnow.scaling = scaling;
			sSnow.position = new scope.Point(0, (rLen * 0.69));

			// Container group for non-rotating elements
            var outer = new scope.Layer();
			outer.locked = true;		// ignore hittest
			
			// Angle read-out
			var txtAngle = new scope.PointText({
				point: [-1 * scale, 18 * scale],
				content: '0',
				fillColor: '#ffffff',
				font: 'freightsans_probold, sans-serif',
				justification: 'center',
				fontSize: 54 * scale
			});
			outer.addChild(txtAngle);

			var txtDegrees = new scope.PointText({
				point: [50 * scale, -2 * scale],
				content: 'o',
				fillColor: '#ffffff',
				font: 'freightsans_prosemibold, sans-serif',
				justification: 'center',
				fontSize: 36 * scale
			});
			outer.addChild(txtDegrees);
			
			// "Up" arrow symbol
			var cloud = controller.notify(m.getSymbol, 'cloud');
			var cCloud = cloud.clone();
			cloud = cCloud.place(new scope.Point(((addNews) ? 0 : 0*10) * scale, -(rLen + ((addNews) ? 64 : 28) * scale)));
			cloud.scaling = scaling;
			outer.addChild(cloud);
		
			// Direction labels
			len = dirLabels.length;
			var targ = (addNews) ? cUiText : cUiTextAlt;	// Hackilicious!
			for (i = 0; i < len; i++)
			{
				dir = dirLabels[i];
				dir[targ] = new scope.PointText({
					point: [0, -(rLen + ((addNews) ? 90 : 50) * scale)],
					content: dir.l,
					fillColor: '#ffffff',
					opacity: 0,
					//weight: 'bold',
					font: 'freightsans_prolight, sans-serif',
					justification: 'center',
					fontSize: 48 * scale
				});

				outer.addChild(dir[targ]);
			}
			
			// Wait for device API libraries to load
			if (checkForCompass)
			{
				hasCompass = (typeof navigator.compass !== 'undefined');
				dbg('Compass available: ' + hasCompass);

				if (hasCompass)
				{
					navigator.compass.getCurrentHeading(onCompassSuccess, onCompassError);
					document.addEventListener('deviceready', onDeviceReady, false);
				}
			}
			
			// Add to passed layer
			var containerLayer = new scope.Layer();
			containerLayer.addChild(inner);
			containerLayer.addChild(outer);
			containerLayer.remove();
			layer.addChild(containerLayer);
			
			// Return new UI component collection
			return { inner: inner
				   , txtDegrees: txtDegrees
				   , txtAngle: txtAngle
				   , layer: layer
				   , outer: outer
				   , container: containerLayer
				   };
		}
		
		function updateUi()
		{
			var rot = 0;
			
			if (!hasCompass)
			{
				rot = angSec * ((Date.now() - offset) % sec60) - 90;
				angleCurrent = Math.round(rot);
			}
			else
			{
				if (angleUpdate === null || isNaN(angleUpdate))
				{
					return false;
				}

				if (Math.round(angleUpdate) === Math.round(angleCurrent))
				{
					return false;
				}
				
				angleCurrent = angleUpdate;
				angleUpdate = null;
				rot = angleCurrent;
			}
			
			if (rot < 0)
			{
				rot += 360;
			}
			
			var w2 = currWidth / 2;
			var gw2 = currGridWidth / 2;
			var range = 45 * 1.5;
			
			for (var i = dirLabels.length - 1; i >= 0; i--)
			{
				var dir = dirLabels[i];
				var mid = dir.p;
				var min = mid - range;
				var max = mid + range;
				var refAngle = rot;

				if (min < 0)
				{
					mid -= min;
					max -= min;
					refAngle = (rot - min) % 360;
					min = 0;
				}
				else if (max > 360)
				{
					if (refAngle < range)
					{
						refAngle += 360;
					}
				}

				if (refAngle < min || refAngle > max)
				{
					dir[cUiText].opacity = 0;
					if (dir[cUiTextAlt])
					{
						dir[cUiTextAlt].opacity = 0;
					}
					continue;
				}

				var d = (mid - refAngle);
				var f = (Math.abs(d) > 5) ? 'freightsans_prolight' : 'freightsans_prosemibold';
				d /= range;
				var o = 1 - 2 * Math.abs(d) + d * d;
				dir[cUiText].opacity = o;
				dir[cUiText].font = f;
				dir[cUiText].point.x = w2 * (0 + d);
				
				if (dir[cUiTextAlt])
				{
					dir[cUiTextAlt].opacity = o;
					dir[cUiTextAlt].font = f;
					dir[cUiTextAlt].point.x = gw2 * (1 + d);
				}
			}
			
			updateCompass(weatherFull, rot, 12);
			updateCompass(weatherGrid, rot, 6);
			
			return true;
        }
		
		function updateCompass(c, rot, offsetDegrees)
		{
            c.inner.rotation = 360 - rot;
			c.txtAngle.content = Math.round(rot) % 360;
			c.txtDegrees.position.x = c.txtAngle.position.x + c.txtAngle.bounds.width / 2 + offsetDegrees;
		}
		

        ///////////////////////////////////////////////////////////////////////////////
        // EVENT HANDLERS
        ///////////////////////////////////////////////////////////////////////////////

		// Device APIs are available
		function onDeviceReady()
		{
			document.removeEventListener('deviceready', onDeviceReady, false);
			startCompass();
		}

		// onCompassSuccess: Get the current heading
		function onCompassSuccess(e)
		{
//			dbg('Heading: ' + e.magneticHeading);
			angleUpdate = e.magneticHeading;
		}

		// onCompassError: Failed to get the heading
		function onCompassError(e)
		{
			dbg('Compass error: ' + e.code);
		}
	}

	module.exports = Weather;
});
