define(function(require, exports, module) {
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var Utils = require('lib/Utils');
    //var Paper = require('paper');

	function Clock()
	{
        // state
        var controller;
		var currWidth;
		var currHeight;
		var currGridWidth;
		var currGridHeight;
		
        // Full view element
        var handLittle;
        var handBig;
        var handSecond;

		// Grid view elements
		var gridTime;

        // settings

        // consts
		var m = Defines.msg;
		var dbg = Utils.dbg('Clock');
        var sec60 = (60 * 1000);
        var angSec = 360 / sec60;
        var min60 = sec60 * 60;
        var angMin = 360 / min60;
        var hr12 = (12 * min60);
        var angHr = 360 / hr12;
        var offset = new Date().getTimezoneOffset() * sec60;

		
        ///////////////////////////////////////////////////////////////////////////////
        // PROPERTIES
        ///////////////////////////////////////////////////////////////////////////////
		
		this.getName = function()
		{
			return 'Clock';
		};
		
		this.setSize = function(w, h, gridWidth, gridHeight)
		{
			currWidth = w;
			currHeight = h;
			currGridWidth = gridWidth;
			currGridHeight = gridHeight;
		};
		
		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

		//
		// Generate full-view UI
		//
        this.generateFullView = function(host, scope, options, layerFull)
        {
            controller = host;
			offset = options.hrsOffset * 60 * sec60;
			
            var i, s, text;
            var rad = 2 * Math.PI / 360;
            var rLen = 120;
            var ang = -90;
            var angAlt = -90;
            var angSecond = -90;
            var start = new scope.Point(0, 0);

            // Outer ring
            var circ = new scope.Path.Circle(new scope.Point(0, 0), rLen);
            circ.style = { strokeColor: '#ffffff', strokeWidth: 1 };
            circ = new scope.Path.Circle(new scope.Point(0, 0), rLen + 4.5);
            circ.style = { strokeColor: '#ffffff', strokeWidth: 3 };
            circ = new scope.Path.Circle(new scope.Point(0, 0), rLen + 9);
            circ.style = { strokeColor: '#ffffff', strokeWidth: 1 };

            // Hours
            for (i = 3; i <= 12; i += 3)
            {
                text = new scope.PointText({
                    //point: [0, 0],
                    content: i,
                    //fillColor: '#ffffff',
                    fillColor: 'rgba(255,255,255,0.07)',
                    //strokeColor: '#a9a7a5',
                    strokeColor: '#ffffff',
                    //fontFamily: 'Asap, Helvetica, Tahoma, sans-serif',
                    font: 'freightsans_probold_italic, Helvetica, Tahoma, sans-serif',
                    //fontWeight: 'bold',
                    justification: 'center',
                    fontSize: 64
                });

                s = (i == 3) ? 0.75 : ((i == 6) ? 0.70 : ((i == 9) ? 0.74 : 0.78));
                text.position = [ rLen * s * Math.cos(2 * Math.PI * i / 12 - Math.PI / 2)
                                , rLen * s * Math.sin(2 * Math.PI * i / 12 - Math.PI / 2) - ((i == 3 || i === 9) ? 10 : 0)];
            }

			if (options.label)
			{
                text = new scope.PointText({
                    point: [0, rLen + 10 + 50],
                    content: options.label,
                    fillColor: '#ffffff',
                    font: 'gibsonsemibold, Helvetica, Tahoma, sans-serif',
                    justification: 'center',
                    fontSize: 48
                });
			}
            // Ticks
            for (i = 0; i < 60; i++)
            {
                s = (!(i % 5)) ? 0.9 : 0.925;
                var tick = new scope.Path({
                    segments: [
                              [ rLen * Math.cos(rad * i * 6), rLen * Math.sin(rad * i * 6) ]
                            , [ rLen * s * Math.cos(rad * i * 6),  rLen * s * Math.sin(rad * i * 6) ]],
                    strokeColor: '#ffffff',
                    strokeWidth: (!(i % 5)) ? 2 : 1
                });
            }

            // Little hand
            handLittle = new scope.Path({
                segments: [[0, 0], [ rLen * 0.5, 3 ], [ rLen * 0.5, 7 ], [0, 10], [0, 0]],
                fillColor: '#ffffff',
                strokeWidth: 1
            });

            handLittle.pivot = new scope.Point(4, handLittle.bounds.center.y);
            handLittle.position = new scope.Point(0, 0);

            // Big hand
            handBig = new scope.Path({
                segments: [[0, 0], [ rLen * 0.8, 3 ], [ rLen * 0.8, 7 ], [0, 10], [0, 0]],
                fillColor: '#ffffff',
                strokeWidth: 1
            });

            handBig.pivot = new scope.Point(4, handBig.bounds.center.y);
            handBig.position = new scope.Point(0, 0);

            // Second hand
            handSecond = new scope.Path({
                segments: [[0, 0], [ rLen * 0.8, 0 ]],
                strokeColor: '#ff0000',
                strokeWidth: 2
            });

            handSecond.pivot = new scope.Point(4, handSecond.bounds.center.y);
            handSecond.position = new scope.Point(0, 0);

            // Pin
            var pin = new scope.Path.Circle(new scope.Point(0, 0), 1);
            pin.style = { fillColor: '#231f20', strokeWidth: 0 };
			
			this.update();
        };
		
		//
		// Generate grid-view UI
		//
		this.generateGridView = function(scope, layerGrid, gridWidth, gridHeight)
		{
			currGridHeight = gridHeight;
			currGridWidth = gridWidth;
			
			gridTime = new scope.PointText({
				content: '--',
				fillColor: '#929497',
				font: 'gibsonregular, sans-serif',
				justification: 'center',
				fontSize: 24,
				point: [ currGridWidth / 2, currGridHeight / 2 ]
			});
		};
		
		var lastMinutes = -1;
		var lastSeconds = -1;
		
		//
		// Tick updates
		//
        this.update = function(e)
        {
            var t = Date.now() - offset;// - 60 * 60 * 1000;

            handSecond.rotation = angSec * (t % sec60) - 90;
            handBig.rotation = angMin * (t % min60) - 90;
            handLittle.rotation = angHr * (t % hr12) - 90;
			
			var dt = new Date(t);
			var secs = dt.getUTCSeconds();
			var mins = dt.getUTCMinutes();
			
			if (!gridTime || secs === lastSeconds)//|| mins === lastMinutes)
			{
				return;
			}
			
			lastSeconds = secs;
			lastMinutes = mins;
			
			var hrs = dt.getUTCHours();
			var ampm = ((hrs >= 12) ? 'pm' : 'am');
			hrs = (hrs == 0) ? 12 : (hrs > 12) ? (hrs - 12) : hrs;
			gridTime.content = hrs + ((secs % 2) ? ':' : ' ') + ((mins < 10) ? '0' : '') + mins + ' ' + ampm;
        };

		
		///////////////////////////////////////////////////////////////////////////////
		// EVENT HANDLERS
		///////////////////////////////////////////////////////////////////////////////
	}

	module.exports = Clock;
});
