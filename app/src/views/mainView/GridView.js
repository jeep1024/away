define(function(require, exports, module)
{
	'use strict';

	var Utils = require('lib/Utils');
	var Defines = require('Defines');

	// import dependencies
	//var Paper = require('paper');
	
	// consts
	var c = Defines.cmd;
	var m = Defines.msg;


	function GridView()
	{
		// state
		var controller;
		var currWidth, currHeight;

		// anims
		
		// elements
		var layer;
		var items = [];

		// settings

		// consts
		var dbg = Utils.dbg('GridView', 0);


		///////////////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////////////

		this.run = function(host, scope, components, w, h)
		{
			controller = host;
			currWidth = w;
			currHeight = h;
			
			// Main layer container
			layer = new scope.Layer();
			layer.transformContent = false;
			layer.pivot = new scope.Point(0, 0);
			
			var len = components.length;
			for (var i = 0; i < len; i++)
			{
				var item = components[i].generateGridView(scope, i);
				items.push(item);
				layer.addChild(item);
			}
			
			return layer;
		};
		
		this.cmd = function(cmd, args)
		{
			var i, len;
			
			switch (cmd)
			{
				case c.resize:
				{
					len = items.length;
					currWidth = args.w;
					currHeight = args.h;
					
					for (i = 0; i < len; i++)
					{
						items[i].setPosition([ currWidth * (0.5 + (i % Defines.numColumns))
											 , currHeight * (0.5 + Math.floor(i / Defines.numRows))
											 ]);
					}
					
					break;
				}
					
				default:
				{
					dbg('Unknown cmd: "' + cmd + '"');
					break;
				}
			}
			
			return null;
		};
		
		this.notify = function(msg, args)
		{
			switch (msg)
			{
				default:
				{
					dbg('Unknown msg: "' + msg + '"');
					break;
				}
			}
			
			return null;
		};


		///////////////////////////////////////////////////////////////////////////////
		// INTERNAL
		///////////////////////////////////////////////////////////////////////////////


		///////////////////////////////////////////////////////////////////////////////
		// EVENT HANDLERS
		///////////////////////////////////////////////////////////////////////////////
		
	}

	module.exports = GridView;
});
