define(function(require, exports, module)
{
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var Utils = require('lib/Utils');
    var Paper = require('paper');

	function Clock()
	{
        // state
        var controller;
		var currWidth;
		var currHeight;
		var currGridWidth;
		var currGridHeight;
		var that = this;
		
        // Container elements
        var layerFull;
		var layerGrid;
		var bgGrid;
		var hitGrid;
		
		// Composition of components
		var component;

        // settings

        // consts
		var dbg = Utils.dbg('ComponentHost');

		
        ///////////////////////////////////////////////////////////////////////////////
        // PROPERTIES
        ///////////////////////////////////////////////////////////////////////////////
		
		this.getFullLayer = function()
		{
			return layerFull;
		};
		
		this.getPosition = function(pos)
		{
			return layerFull.position;
		};

		this.setPosition = function(pos)
		{
            layerFull.position = pos;
		};
		
		this.setSize = function(w, h, gridWidth, gridHeight)
		{
			currWidth = w;
			currHeight = h;
			currGridWidth = gridWidth;
			currGridHeight = gridHeight;
			
			if (component)
			{
				component.setSize(currWidth, currHeight, gridWidth, gridHeight);
			}
			
			if (layerFull)
			{
				layerFull.pivot = [0, 0];
			}
			
			if (layerGrid)
			{
				hitGrid.point = [ -gridWidth / 2, -gridHeight / 2 ];
				hitGrid.size = [ gridWidth, gridHeight ];
				bgGrid.point = hitGrid.point;
				bgGrid.size = hitGrid.size;
			}
		};
		
		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.init = function(host, scope, ComponentClass, options)
        {
            controller = host;
			
            // Layer container -- fullView
            layerFull = new Paper.Layer();
            layerFull.transformContent = false;
			
			// Set up the generated component class
			component = new ComponentClass();
			component.setSize(currWidth, currHeight, currGridWidth, currGridHeight);
			component.generateFullView(controller, scope, options, layerFull);

			return layerFull;
        };
		
		this.generateGridView = function(scope, id)
		{
			// Layer container -- gridView
			layerGrid = new scope.Layer();
			layerGrid.transformContent = false;
			
			var cfgRect = { point: [ 0, 0 ]
						  , size: [ (currGridWidth || 200), (currGridHeight || 250) ]
						  , fillColor: '#ffffff'
						  , opacity: (id % 2) ? 0.025 : 0.01
						  , blendMode: 'difference'
						  };
			
			// Background support
			bgGrid = new scope.Shape.Rectangle(cfgRect);
			
			// Component UI
			component.generateGridView(scope, layerGrid, currGridWidth, currGridHeight);
			
			var hdr = new scope.PointText({
				content: component.getName(),
				fillColor: '#929497',
				font: 'freightsans_promedium, sans-serif',
				justification: 'center',
				fontSize: 12,
				point: [currGridWidth / 2, 14]
			});
			
			// Hit rect for grid item
			cfgRect.opacity = 0.0;
			hitGrid = new scope.Shape.Rectangle(cfgRect);
			
			hitGrid.onMouseDown = onBgDown;			
			hitGrid.onMouseUp = onBgUp;
			
			return layerGrid;
		};
		
        this.update = function(e)
        {
			component.update(e);
        };

		
		///////////////////////////////////////////////////////////////////////////////
		// EVENT HANDLERS
		///////////////////////////////////////////////////////////////////////////////
		
		function onBgDown(e)
		{
			hitGrid.opacity = 1.0;
		}

		function onBgUp(e)
		{
			hitGrid.opacity = 0.0;
			controller.notify(Defines.msg.selectItem, that);
		}
	}

	module.exports = Clock;
});
