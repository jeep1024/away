define(function(require, exports, module) {
	'use strict';

	var Utils = require('lib/Utils');
	var Defines = require('Defines');
	
	var ComponentHost = require('views/mainView/ComponentHost');
	var GridView = require('views/mainView/GridView');
	var Clock = require('views/mainView/components/Clock');
	var Compass = require('views/mainView/components/Compass');
	var Weather = require('views/mainView/components/Weather');

	// import dependencies
	var $ = require('jquery');
	var Paper = require('paper');
	var View = require('famous/core/View');
	var Surface = require('famous/core/Surface');
    var Transform = require('famous/core/Transform');
    var StateModifier = require('famous/modifiers/StateModifier');
    var Modifier = require('famous/core/Modifier');
    var Transitionable = require('famous/transitions/Transitionable');
	var RenderController = require('famous/views/RenderController');
	var Easing = require('famous/transitions/Easing');
	
	var PaperSymbols = require('models/PaperSymbols');
	
	var components = [
		{ cl: Compass, options: {} }
		,
		{ cl: Clock, options: { hrsOffset: 8, label: 'Seattle' } }//*/
		,
		{ cl: Weather, options: {} }
	];

	console.log('Paper v(' + Paper.version + ')');

	function MainCanvas()
	{
		// state
		var activeItem;
		var controller;
		var currWidth, currHeight;
		var showingGridView = false;
		var symbols;
		var timerUpdate;
		
		// famo.us elements
		var renderController;
		var bgFull;
		var bgGrid;
		
		// paperjs elements
		var scopeFull;
		var scopeGrid;
		var layerFull;
		var layerGrid;
		var canvasFull;
		var canvasGrid;
		var items = [];
		var gridView;

		// settings
		var size = [ undefined, undefined ];

		// consts
		var c = Defines.cmd;
		var m = Defines.msg;
		var that = this;
		var cNumCols = Defines.numColumns;
		var cNumRows = Defines.numRows;
		var cStartItem = 0;
		var dbg = Utils.dbg('MainCanvas', 0);
		
		// ctor
		View.apply(this, arguments);

		// Define the overall size for hosting context modifier update
		this.setOptions({size: size});
		
        var options =
        {
            openPosition: Defines.widthOptions,
            transition: {
                duration: 300,
                curve: 'easeOut'
            },
            posThreshold: 138,
            velThreshold: 0.75
        };

		
		///////////////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////////////

		this.run = function(host)
		{
			controller = host;

			canvasFull = document.createElement('canvas');
			$(canvasFull).attr('resize', 'true');
			canvasGrid = document.createElement('canvas');
			$(canvasGrid).attr('resize', 'true');
			$(canvasGrid).css({ display: 'none' });	// Hide initially

			// Create an empty project and view for each canvas
			scopeFull = new Paper.PaperScope();
			scopeFull.setup(canvasFull);
			scopeGrid = new Paper.PaperScope();
			scopeGrid.setup(canvasGrid);
			
			// Main layer container
			layerFull = new scopeFull.Layer();
			layerFull.transformContent = false;
			layerFull.pivot = new scopeFull.Point(0, 0);

			// Gird layout
			gridView = new GridView();
			
			// Load assets first
			symbols = new PaperSymbols();
			symbols.load(scopeFull, completedAssetLoad);

			// Init famo.us surface to size canvas
			bgFull = new Surface({ size: size, content: canvasFull, properties: { backgroundColor: '#231f20' }});
			bgGrid = new Surface({ size: size, content: canvasGrid, properties: { backgroundColor: '#231f20' }});
			
			var transition = { curve: Easing.inQuad, duration: 300 };
			renderController = new RenderController({
				inTransition: transition,
				outTransition: transition,
				overlap: true
			});
			
			// Set up default translation transforms
			renderController.inTransformFrom(translateOriginOnly);
			renderController.outTransformFrom(translateRtl);
			renderController.inOpacityFrom(translateOpacity);
			renderController.outOpacityFrom(translateOpacity);
			
			// Force both to display to get layering correct
			var t = $.extend({}, transition);
			t.duration = 10;
			renderController.show(bgGrid, t);
			renderController.show(bgFull, t);
			
			this.add(renderController);
		};
		
		this.cmd = function(cmd, args)
		{
			switch (cmd)
			{
				case c.showGrid:
				{
					if (showingGridView === args)
					{
						break;
					}
					
					if (timerUpdate)
					{
						clearInterval(timerUpdate);
						timerUpdate = null;
					}
					
					showingGridView = args;
					
                    if (showingGridView)
                    {
						$(canvasGrid).css({ display: 'block' });	// FIXME: Ugh (gridview is drawn during init, otherwise)
						renderController.inTransformFrom(translateLtr);
						renderController.outTransformFrom(translateRtl);
						renderController.show(bgGrid, null, finalizeScroll);
                    }
                    else
                    {
						renderController.inTransformFrom(translateRtl);
						renderController.outTransformFrom(translateLtr);
						renderController.show(bgFull, null, finalizeScroll);
                    }

					scopeFull.view.onFrame = null;
					scopeGrid.view.onFrame = null;
					updateItems();
					
					break;
				}
				
				default:
				{
					dbg('Unknown cmd: "' + cmd + '"');
					break;
				}
			}
			
			return null;
		};
		
		this.notify = function(msg, args)
		{
			switch (msg)
			{
				case m.getSymbol:
				{
					return symbols.getSymbol(args);
				}
				
				case m.selectItem:
				{
					activeItem.getFullLayer().remove();
					activeItem = args;
					layerFull.addChild(activeItem.getFullLayer());
					controller.notify(m.clickedTile, args);
					break;
				}
					
				default:
				{
					dbg('Unknown msg: "' + msg + '"');
					break;
				}
			}
			
			return null;
		};


		///////////////////////////////////////////////////////////////////////////////
		// INTERNAL
		///////////////////////////////////////////////////////////////////////////////

		function repositionItems()
		{
			var gw = currWidth / cNumCols;
			var gh = currHeight / cNumRows;
			var len = items.length;
			
			for (var i = 0; i < len; i++)
			{
				items[i].setSize(currWidth, currHeight, gw, gh);
			}
			
			layerFull.setPosition(new scopeFull.Point(currWidth / 2, currHeight / 2));
			if (layerGrid)
			{
				layerGrid.setPosition(new scopeGrid.Point(0*currWidth, 0));
			}
			
			gridView.cmd(c.resize, { w: gw, h: gh });
		}
		
		function completedAssetLoad()
		{
			var len = components.length;
			for (var i = 0; i < len; i++)
			{
				var item = new ComponentHost();
				items.push(item);
				
				// Initial full layers are removed, except for the default one
				if (i === cStartItem)
				{
					activeItem = item;
				}
			}
			
			scopeFull.view.onResize = onResize;
			scopeFull.view.onFrame = checkCanvasSize;

			// Remove all items until we get proper size info
			layerFull.remove();
		}
		
		function updateItems()
		{
			var len = items.length;
			for (var i = 0; i < len; i++)
			{
				items[i].update();
			}
			
			scopeFull.view.draw();
			scopeGrid.view.draw();
		}


		///////////////////////////////////////////////////////////////////////////////
		// EVENT HANDLERS
		///////////////////////////////////////////////////////////////////////////////

		function onResize(e)
		{
			var cvs = $(canvasFull);
			var w = cvs.width();
			var h = cvs.height();
			//dbg('resize - ' + w + ',' + h);

			if (w === currWidth && h === currHeight)
			{
				return;
			}

			currWidth = w;
			currHeight = h;

			repositionItems();
		}

		function checkCanvasSize()
		{
			var cvs = $(canvasFull);
			var w = cvs.width();
			var h = cvs.height();

			if (w === 0 || h === 0)
			{
				dbg('empty canvas: ' + w + ',' + h);
				return;
			}

			currWidth = w;
			currHeight = h;

			repositionItems();
			
			// Once sizes are known, init the UI
			var len = items.length;
			for (var i = 0; i < len; i++)
			{
				var comp = components[i];
				var item = items[i];
				var l = item.init(that, scopeFull, comp.cl, comp.options);
				l.remove();
				
				// Initial full layers are removed, except for the default one
				if (i === cStartItem)
				{
					layerFull.addChild(l);
				}
			}
			
			scopeFull.project.activeLayer.addChild(layerFull);

			// Add in the grid layer
			layerGrid = gridView.run(that, scopeGrid, items, w / cNumCols, h / cNumRows);
			//layerGrid.remove();
			scopeGrid.project.activeLayer.addChild(layerGrid);
			
			repositionItems();
			updateItems();
			
			scopeFull.view.onFrame = updateActiveItem;
			controller.notify(m.assetsLoaded, that);
		}

		function updateActiveItem()
		{
			if (Defines.demo < 2)
			{
				activeItem.update();
			}
		}
		
		
		///////////////////////////////////////////////////////////////////////////////
		// TRANSLATIONS (RenderController)
		///////////////////////////////////////////////////////////////////////////////
		
		function translateOriginOnly(progress)
		{
			return 0;
		}
		
		function translateOpacity(progress)
		{
			return 1;
		}
		
		function translateLtr(progress)
		{
			return Transform.translate(currWidth * (1.0 - progress), 0, 0);
		}
		
		function translateRtl(progress)
		{
			return Transform.translate(currWidth * (progress - 1.0), 0, 0);
		}
		
		function finalizeScroll()
		{
			if (showingGridView)
			{
				scopeGrid.view.onFrame = updateActiveItem;
				timerUpdate = setInterval(updateItems, 1000);
			}
			else
			{
				scopeFull.view.onFrame = updateActiveItem;
			}
		}
	}

	// Link to Famo.us View baseclass
	MainCanvas.prototype = Object.create(View.prototype);
	MainCanvas.prototype.constuctor = MainCanvas;

	module.exports = MainCanvas;
});
