define(function(require, exports, module) {
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var View = require('famous/core/View');
    var Modifier = require('famous/core/Modifier');
    var Surface = require('famous/core/Surface');
    var ImageSurface = require('famous/surfaces/ImageSurface');

	function SelectTile()
	{
		// consts
		var m = Defines.msg;
		
        // state
        var controller;
        var timerPress;

        // elements
        var btn;

        // settings
        var btnHeight = 28;
        var urlBtnDown = '/content/images/tileDown.png';
        var urlBtnUp = '/content/images/tile.png';

        // ctor
		View.apply(this, arguments);

        // Define the overall size for hosting context modifier update
        this.setOptions({ size: [btnHeight, btnHeight] });

		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.run = function(host)
        {
            controller = host;

            btn = new ImageSurface({ size: [btnHeight, btnHeight], content: urlBtnUp, properties: { opacity: 0.5 }});
            btn.mod = new Modifier({ origin: [1, 1], align: [1, 1] });
			btn.on(Defines.eventDown, downButton);

			this.add(btn.mod).add(btn);
        };

        function downButton()
        {
            if (timerPress)
			{
				clearTimeout(timerPress);
			}

            btn.setOptions({content: urlBtnDown });
            timerPress = setTimeout(outButton, 300);
            controller.notify(m.clickedTile, true);
        }

        function outButton()
        {
            if (timerPress)
			{
				clearTimeout(timerPress);
			}

            btn.setOptions({content: urlBtnUp });
        }
	}

    // Link to Famo.us View baseclass
    SelectTile.prototype = Object.create(View.prototype);
    SelectTile.prototype.constuctor = SelectTile;

	module.exports = SelectTile;
});
