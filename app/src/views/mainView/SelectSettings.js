define(function(require, exports, module) {
    'use strict';

    // import dependencies
	var Defines = require('Defines');
	var View = require('famous/core/View');
    var Modifier = require('famous/core/Modifier');
    var Surface = require('famous/core/Surface');
    var ImageSurface = require('famous/surfaces/ImageSurface');

	function SelectSettings()
	{
		// aliases
		var m = Defines.msg;
		
        // state
        var controller;
        var timerPress;

        // elements
        var btn;

        // settings
        var btnHeight = 28;
        var urlBtnDown = '/content/images/hamburgerDown.png';
        var urlBtnUp = '/content/images/hamburger.png';

        // ctor
		View.apply(this, arguments);

        // Define the overall size for hosting context modifier update
        this.setOptions({ size: [btnHeight, btnHeight] });

		
        ///////////////////////////////////////////////////////////////////////////////
        // PUBLICS
        ///////////////////////////////////////////////////////////////////////////////

        this.run = function(host)
        {
            controller = host;

            btn = new ImageSurface({ size: [btnHeight, btnHeight], content: urlBtnUp, properties: { opacity: 0.5 }});
            btn.mod = new Modifier({ origin: [1, 0], align: [1, 0] });
			btn.on(Defines.eventDown, downButton);

			this.add(btn.mod).add(btn);
        };

        function downButton()
        {
            if (timerPress)
			{
				clearTimeout(timerPress);
			}

            btn.setOptions({content: urlBtnDown });
            timerPress = setTimeout(outButton, 300);
            controller.notify(m.clickedSettings, true);
        }

        function outButton()
        {
            if (timerPress)
			{
				clearTimeout(timerPress);
			}

            btn.setOptions({content: urlBtnUp });
        }
	}

    // Link to Famo.us View baseclass
    SelectSettings.prototype = Object.create(View.prototype);
    SelectSettings.prototype.constuctor = SelectSettings;

	module.exports = SelectSettings;
});
