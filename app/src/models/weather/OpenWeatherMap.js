define(function(require, exports, module)
{
	'use strict';
	
	var Defines = require('Defines');
	var Utils = require('lib/Utils');
	var m = Defines.msg;
	
	
	function OpenWeatherMap(controller, cfg)
	{
		// consts
        var dbg = Utils.dbg('WeatherLoader');

		// state
		var callbackCompleted;
		var baseUrl = cfg.baseUrl;
		var apiKey = cfg.apiKey;
		var intervalUpdate = cfg.intervalUpdate;
		var intervalRetry = cfg.intervalRetry;
		
		
		///////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////
		
		this.init = function(cfgInit)
		{
			loadPlaceSearch(cfgInit);
		};

		this.loadCurrent = function(request)
		{
			// For now, always request data
			loadCurrentWeather(request);
		};
		
		this.load3hrForecast = function(request)
		{
			load3HourForecast(request);
		};
		
		this.loadDailyForecast = function(request)
		{
			loadDailyForecast(request);
		};
		
		
		///////////////////////////////////////////////////////////////////////
		// INTERNAL
		///////////////////////////////////////////////////////////////////////
		
		function loadCurrentWeather(request)
		{
			// FIXME: Allow multiple cities
			// FIXME: Allow city ids (for now, just assume CITY_NAME,COUNTRY|STATE)
			var urlCurrent = baseUrl + 'weather?callback=?';
			$.getJSON(urlCurrent, {
				APPID: apiKey,
				q: request.city + ((request.stateCountry) ? (',' + request.stateCountry) : '')
			})
			.done(function(data) {
				dbg('Got weather data', data);
			})
			.fail(function(err) {
				dbg('Error: ', err)
			})
			.always(function(data, msgResult) {
				dbg('COMPLETE');// - DATA', data);
				dbg('RESULT', msgResult);
			});
			
			dbg('Loaded weather');
			//callbackCompleted();
		}
		
		function load3HourForecast(request)
		{
			// FIXME: Is 5day only
			// FIXME: Allow multiple cities
			// FIXME: Allow city ids (for now, just assume CITY_NAME,COUNTRY|STATE)
			var urlCurrent = baseUrl + 'forecast?cnt=3&callback=?';
			$.getJSON(urlCurrent, {
				APPID: apiKey,
				q: request.city + ((request.stateCountry) ? (',' + request.stateCountry) : '')
			})
			.done(function(data) {
				dbg('Got 3-hour data', data);
			})
			.fail(function(err) {
				dbg('Error: ', err)
			})
			.always(function(data, msgResult) {
				dbg('COMPLETE');// - DATA', data);
				dbg('RESULT', msgResult);
			});
			
			dbg('Loaded weather');
			//callbackCompleted();
		}
		
		function loadDailyForecast(request)
		{
			// FIXME: Is 5day only
			// FIXME: Allow multiple cities
			// FIXME: Allow city ids (for now, just assume CITY_NAME,COUNTRY|STATE)
			var urlCurrent = baseUrl + 'forecast/daily?cnt=7&callback=?';
			$.getJSON(urlCurrent, {
				APPID: apiKey,
				q: request.city + ((request.stateCountry) ? (',' + request.stateCountry) : '')
			})
			.done(function(data) {
				dbg('Got daily forecast data', data);
			})
			.fail(function(err) {
				dbg('Error: ', err)
			})
			.always(function(data, msgResult) {
				dbg('COMPLETE');// - DATA', data);
				dbg('RESULT', msgResult);
			});
			
			dbg('Loaded weather');
			//callbackCompleted();
		}
		
		function loadPlaceSearch(request)
		{
			// http://www.geonames.org/export/geonames-search.html
			// FIXME: Is 5day only
			// FIXME: Allow multiple cities
			// FIXME: Allow city ids (for now, just assume CITY_NAME,COUNTRY|STATE)
			var urlCurrent = 'http://api.geonames.org/searchJSON?maxRows=10&username=jkeimig';
			$.getJSON(urlCurrent, {
				APPID: apiKey,
				q: request.city + ((request.stateCountry) ? (',' + request.stateCountry) : '')
			})
			.done(function(data) {
				dbg('Got place search data', data);
				dbg('geonames.length=' + data.geonames.length);
			})
			.fail(function(err) {
				dbg('Error: ', err)
			})
			.always(function(data, msgResult) {
				dbg('COMPLETE');// - DATA', data);
				dbg('RESULT', msgResult);
			});
			
			dbg('Loaded place search');
			//callbackCompleted();
		}
	}

	module.exports = OpenWeatherMap;
});
