define(function(require, exports, module)
{
	'use strict';
	
	var Defines = require('Defines');
	var Utils = require('lib/Utils');
	var OpenWeatherMap = require('models/weather/OpenWeatherMap');
	
	var c = Defines.cmd;
	var m = Defines.msg;
	
	
	function WeatherLoader(controller)
	{
        var dbg = Utils.dbg('WeatherLoader');
		
		// state
		var currWeatherApi = new OpenWeatherMap(this, Defines.weather.providers.openweathermap);
		
		
		///////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////
		
		this.init = function(cfgInit)
		{
			currWeatherApi.init(cfgInit);
			currWeatherApi.loadCurrent(cfgInit);
			currWeatherApi.load3hrForecast(cfgInit);
			currWeatherApi.loadDailyForecast(cfgInit);
		};
		
		this.notify = function(msg, args)
		{
			switch (msg)
			{
				case m.getValue:
				case m.setValue:
				{
					return controller.notify(msg, args);
				}
					
				default:
				{
					dbg('Unknown msg "' + msg + '"', args);
					break;
				}
			}
			
			return null;
		};
		
		
		///////////////////////////////////////////////////////////////////////
		// INTERNAL
		///////////////////////////////////////////////////////////////////////
		
	}

	module.exports = WeatherLoader;
});
