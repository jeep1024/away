define(function(require, exports, module)
{
	'use strict';
	
	var Defines = require('Defines');
	var Paper = require('paper');

	function PaperSymbols()
	{
		// state
		var callbackCompleted;
		var cntLoad = 0;
		var scope;
		
		// consts
		var symbols = {
			  arrowUp: { path: 'arrowUp', s: null, failed: false }
			, triangle: { path: 'triangle', s: null, failed: false }
			, compassInnerDown: { path: 'compass_inner_down', s: null, failed: false }
			, cloud: { path: 'cloud0', s: null, failed: false }
			, snow: { path: 'snow0', s: null, failed: false }
			, sun: { path: 'sun0', s: null, failed: false }
			, raindrop: { path: 'raindrop0', s: null, failed: false }
		};
		
		
		///////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////
		
		this.load = function(currScope, callback)
		{
			cntLoad = 0;
			callbackCompleted = callback;
			scope = currScope;
			
			loadSymbols();
		};
		
		this.getSymbol = function(id)
		{
			return symbols[id].s.clone();
		};
		
		
		///////////////////////////////////////////////////////////////////////
		// INTERNAL
		///////////////////////////////////////////////////////////////////////
		
		function loadSymbols()
		{
			for (var id in symbols)
			{
				var o = symbols[id];
				if (o.s || o.failed)
				{
					continue;
				}
				//console.log('Loading: ' + o.path + ' - D=' + Defines);
				cntLoad++;
				scope.project.importSVG(Defines.symbolPath + o.path + Defines.symbolExt,
					{
						expandShapes: false,
						onLoad: loadedItem(o)
					});
				
				return;
			}
			
			console.log('Loaded ' + cntLoad + ' symbols');
			callbackCompleted();
		}
		
		function loadedItem(o)
		{
			return function(item)
			{
				//console.log('item: len=' + item.children[0].children.length);
				o.s = new scope.Symbol(item);
				item.remove();
				loadSymbols();
			};
		}
	}

	module.exports = PaperSymbols;
});
