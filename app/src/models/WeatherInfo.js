define(function(require, exports, module)
{
	'use strict';
	
	function WeatherInfo(cfg)
	{
		// properties
		/*
		this.sunrise = newCfg.sunrise || 0;
		this.sunset = newCfg.sunset || 0;

		this.tempCurr = newCfg.tempCurr || null;
		this.tempMin = newCfg.tempMin || null;
		this.tempMax = newCfg.tempMax || null;

		this.pressureCurr = newCfg.pressureCurr || null;
		this.pressureSea = newCfg.pressureSea || null;
		this.pressureGround = newCfg.pressureGround || null;

		this.humidity = newCfg.pressure || null;

		this.windSpeed = newCfg.windSpeed || null;
		this.windDir = newCfg.windDir || null;

		// ?
		this.clouds = newCfg.clouds || null;

		this.rainTime = newCfg.rainTime || null;
		this.rainStatus = newCfg.rainStatus || null;

		this.locationName = newCfg.locationName || '<unknown>';
		this.locationId = newCfg.locationId || 0;
		this.locationLat = newCfg.locationLat || 0;
		this.locationLng = newCfg.locationLng || 0;

		this.timestamp = newCfg.timestamp || 0;
		this.status = newCfg.status || 0;
		*/
		
		this.sunrise = 0;	// UTC getTime
		this.sunset = 0;	// UTC getTime

		this.tempCurr = null;
		this.tempMin = null;
		this.tempMax = null;

		this.pressureCurr = null;
		this.pressureSea = null;
		this.pressureGround = null;

		this.humidity = null;

		this.windSpeed = null;
		this.windDir = null;

		// ?
		this.clouds = null;

		this.rainTime = null;
		this.rainStatus = null;

		this.locationName = '?? unknown ??';
		this.locationId = 0;
		this.locationLat = 0;
		this.locationLng = 0;

		this.timestamp = 0;		// UTC getTime
		this.status = 0;		// HTTP status code
		
		
		///////////////////////////////////////////////////////////////////////
		// PUBLICS
		///////////////////////////////////////////////////////////////////////
		
		this.update = function(newCfg)
		{
			$.extend(this, newCfg);
		}
		
		
		this.update(cfg);
	}

	module.exports = WeatherInfo;
});
