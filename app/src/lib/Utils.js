define(function(require, exports, module)
{
	'use strict';

	var Utils =
	{
		dbg: function(id, minLevel)
		{
			return function(info, data, level)
			{
				// Never allow logging if minLevel < 0
				// Allow all logging if minLevel is !truthy
				// Otherwise, must match min level on a log request
				if (!minLevel || (minLevel >= 0 && level >= minLevel))
				{
					console.log('[' + id + '] ' + info + (((arguments.length > 1) && (': ' + JSON.stringify(data))) || ''));
				}
			};
		}
	};
	
	module.exports = Utils;
});
