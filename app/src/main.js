/* globals define */
define(function(require, exports, module)
{
    'use strict';

	console.log('############## init ##############');
    // import dependencies
	var ViewManager = require('controllers/ViewManager');
//    var Paper = require('paper');

    //console.log('statusbar: ' + (typeof window.StatusBar));
    if (typeof window.StatusBar != 'undefined')
    {
        window.StatusBar.styleDefault();
    }

    // Immediate kick-off
    var vm = new ViewManager();
    vm.init();
	
//	var str = '';
//	for (var i = 0; i < 65535; i++)
//	{
//		var d = 0x10000 + i;
//		
//		str += '&#x' + d.toString(16) + ';';
//		if (!(i % 1024))
//		{
//			console.log('Chunk: ' + i);
//		}
//		//console.log('#x' + d.toString(16));
//	}
//	
//	$('#fonts').html(str);
});
