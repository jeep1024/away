#!/bin/bash

set -e

echo "***** GRUNTING *****"
grunt
echo "***** GRUNT COMPLETE *****"


rm -rf ../../exp/Away/www/css
rm -rf ../../exp/Away/www/content
rm -rf ../../exp/Away/www/src
cp -a dist/css ../../exp/Away/www
cp -a dist/src ../../exp/Away/www
cp -a dist/content ../../exp/Away/www
cp dist/index.html ../../exp/Away/www
echo "***** COPY COMPLETE *****"

pushd ../../exp/Away/www
cca prepare
cca run ios
echo "***** CCA PREPARE COMPLETE *****"
popd
